#Programa que calcula la suma de los primeros n enteros usando 1+2+3+...+n=(n+1)n/2 y pidiendo la n al usuario, manejo la excepción de un input inválido
cont = True #Establecemos una bandera para salir del bucle de verificación
while(cont): #Iniciamos el bucle de verificación
    n = input("Ingresa el entero de tu elección: ") #Solicitamos un entero y lo guardamos en la variable n
    if n.isdigit(): #Revisamos que n sea un entero positivo, y si sí lo es
        cont = False #Rompemos el bucle
    else: #De lo contrario, nos quejamos y regresamos al bucle"
        print("Eso no es un entero positivo...")
print("Computando la suma de los primeros ", n ," enteros...") #Avisamos que vamos a computar la suma usando dicho entero
suma = (int(n) + 1)*int(n)/2 #Operamos n utilizando la fórmula anterior, sin olvidar castear a Integer lo que hay en n (que era String)
print(int(suma)) #Imprimimos el resultado casteándolo a Integer para evitar un .0 decimal innecesario.
