#Programa que calcula la suma de n=1,2,3,...,100 usando 1+2+3+...+n=(n+1)n/2
n = 100 #Establecemos n
suma = (n + 1)*n/2 #Operamos n utilizando la fórmula anterior
print(int(suma)) #Imprimimos el resultado casteándolo a Integer para evitar un .0 decimal innecesario.
