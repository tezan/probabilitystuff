#Programa que calcula la suma de los primeros n enteros usando 1+2+3+...+n=(n+1)n/2 y pidiendo la n al usuario
  
n = input("Ingresa el entero de tu elección: ") #Solicitamos un entero y lo guardamos en la variable n
print("Computando la suma de los primeros ", n ," enteros...") #Avisamos que vamos a computar la suma usando dicho entero
suma = (int(n) + 1)*int(n)/2 #Operamos n utilizando la fórmula anterior, sin olvidar castear a Integer lo que hay en n (que era String)
print(int(suma)) #Imprimimos el resultado casteándolo a Integer para evitar un .0 decimal innecesario.
